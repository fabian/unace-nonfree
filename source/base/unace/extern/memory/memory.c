#define INCL_BASE_MEMORY

#include "base/unace/includes.h"


/*-----------------BASE_MEMORY_EXTERN_OptimizeOtherMemory----------------*/

void    BASE_MEMORY_EXTERN_OptimizeOtherMemory(void)
{
}

/*-----------------BASE_MEMORY_EXTERN_MaxMemoryRequirement---------------*/

ULONG   BASE_MEMORY_EXTERN_MaxMemoryRequirement(void)
{
  return 16*1024*1024;
}
