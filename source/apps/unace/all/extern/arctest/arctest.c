#define INCL_BASE_ARCHIVES_TEST
#define INCL_BASE_DIRDATA

#include "base/all/includes.h"


/*-----------------BASE_ARCHIVES_TEST_EXTERN_ArchiveTestBreak------------*/

BOOL    BASE_ARCHIVES_TEST_EXTERN_ArchiveTestBreak(BOOL IgnoreKeys,
                                                   INT Sectors)
{
  return 1;
}

/*-----------------BASE_ARCHIVES_TEST_EXTERN_ArchiveTestOtherFormats-----*/

void    BASE_ARCHIVES_TEST_EXTERN_ArchiveTestOtherFormats(PINT IsArchive,
                                                          INT FilePos,
                                                          pBASE_DIRDATA_DirData TempDir)
{
}
